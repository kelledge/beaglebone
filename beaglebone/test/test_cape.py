import unittest
import struct

from beaglebone.cape import (
    CapePinUsage,
    CapeIdentity
)    

PIN_USAGE_FIXTURE = struct.pack('>H', int('1100000001110110', 2))

class TestCapePinUsage(unittest.TestCase):
    def setUp(self):
        self.cape_pin_usage = CapePinUsage.from_bytes(PIN_USAGE_FIXTURE)

    def test_used(self):
        self.assertEqual(self.cape_pin_usage.used, 1)

    def test_mode(self):
        self.assertEqual(self.cape_pin_usage.mode, 6)

class TestCapeIdentity(unittest.TestCase):

    def setUp(self):
        self.cape_identity = CapeIdentity()
