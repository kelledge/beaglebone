import unittest
import binascii
import ctypes
import StringIO

from beaglebone.bootloader import (
    UBootImageHeader, 
    SPLImageHeader
)

from beaglebone.utils import (
    chunk_size_generator,
    chunk_file_reader
)


UBOOT_FIXTURE_1 = {
    "data": """270519564e7da34d53d0f1530006ce60
               8080000000000000515263fe11020500
               552d426f6f7420323031342e30372d30
               303032332d67333336346231382d6469""",

    "summary": """Image Name:   U-Boot 2014.07-00023-g3364b18-di
                  Created:      Thu Jul 24 06:43:15 2014
                  Image Type:   ARM U-Boot Firmware (uncompressed)
                  Data Size:    446048 Bytes = 435.59 kB = 0.43 MB
                  Load Address: 80800000
                  Entry Point:  00000000""",

    "fields": {
        "ih_magic": 0x27051956,
        "ih_hcrc": 0x4e7da34d,
        "ih_time": 0x53d0f153,
        "ih_size": 0x0006ce60,
        "ih_load": 0x80800000,
        "ih_ep": 0x00000000,
        "ih_dcrc": 0x515263fe,
        "ih_os": 0x11,
        "ih_arch": 0x02,
        "ih_type": 0x05,
        "ih_comp": 0x00,
#        "ih_name": "U-Boot 2014.07-00023-g3364b18-di"
    }
}

UBOOT_FIXTURE_2 = {
    "data": """27051956aeac1817560a04b20004d0bc
               808000000000000005e91b8711020500
               552d426f6f7420323031352e31302d72
               63332d30303231332d67316662386437""",

    "summary": """Image Name:   U-Boot 2015.10-rc3-00213-g1fb8d7
                  Created:      Mon Sep 28 22:25:38 2015
                  Image Type:   ARM U-Boot Firmware (uncompressed)
                  Data Size:    315580 Bytes = 308.18 kB = 0.30 MB
                  Load Address: 80800000
                  Entry Point:  00000000""",

    "fields": {
        "ih_magic": 0x27051956,
        "ih_hcrc": 0xaeac1817,
        "ih_time": 0x560a04b2,
        "ih_size": 0x0004d0bc,
        "ih_load": 0x80800000,
        "ih_ep": 0x00000000,
        "ih_dcrc": 0x05e91b87,
        "ih_os": 0x11,
        "ih_arch": 0x02,
        "ih_type": 0x05,
        "ih_comp": 0x00,
#        "ih_name": "U-Boot 2014.07-00023-g3364b18-di"
    }
}

SPL_FIXTURE = {
    "data": """400000000c000000000000000000000000000000434853455454494e475300
               00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
               ffffc1c0c0c000010000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               00000000000000000000000000000000000000000000000000000000000000
               0000000000000000000000000000000020f8000000042f40""",

    "summary": """Section CHSETTINGS offset 40 length c
                  CHSETTINGS (c0c0c0c1) valid:0 version:1 reserved:0 flags:0
                  GP Header: Size f820 LoadAddr 402f0400""",

    "ch_toc_fields": {
        "section_offset": 0x00000040,
        "section_size": 0x0000000c,
#        "unused": "",
#        "section_name": "CHSETTINGS"
    },

    "ch_settings_fields": {
        "section_key": 0xc0c0c0c1,
        "valid": 0x00,
        "version": 0x01,
        "reserved": 0x00,
        "flags": 0x00
    },

    "gp_header_fields": {
        "size": 0xf820,
        "load_addr": 0x402f0400
    }
}


class TestSPLImageHeader(unittest.TestCase):
    def setUp(self):
        self.spl_data = binascii.unhexlify(
                              SPL_FIXTURE["data"].replace("\n", "").replace(" ", "")
                          )
        self.spl_image_header = SPLImageHeader(StringIO.StringIO(self.spl_data))

    def test_ch_toc_fields(self):
        for field, expected_value in SPL_FIXTURE["ch_toc_fields"].iteritems():
            actual_value = getattr(self.spl_image_header.ch_toc, field)
            self.assertEqual(actual_value, expected_value, "%s is incorrect" % field)

    def test_ch_settings_fields(self):
        for field, expected_value in SPL_FIXTURE["ch_settings_fields"].iteritems():
            actual_value = getattr(self.spl_image_header.ch_settings, field)
            self.assertEqual(actual_value, expected_value, "%s is incorrect." % field)

    def test_gp_header_fields(self):
        for field, expected_value in SPL_FIXTURE["gp_header_fields"].iteritems():
            actual_value = getattr(self.spl_image_header.gp_header, field)
            self.assertEqual(actual_value, expected_value, "%s is incorrect. %s != %s" % (field, actual_value, expected_value))

    def test_verify(self):
        self.assertTrue(self.spl_image_header.verify())

        ch_toc = self.spl_image_header.ch_toc
        ch_settings = self.spl_image_header.ch_settings
        gp_header = self.spl_image_header.gp_header

        ctypes.memmove(ctypes.addressof(ch_toc.section_name),
                       "\x00" * 12,
                       ctypes.sizeof(ch_toc.section_name))

        ctypes.memmove(ctypes.addressof(ch_settings),
                       "\x00" * ctypes.sizeof(ch_settings),
                       ctypes.sizeof(ch_settings))

        ctypes.memmove(ctypes.addressof(gp_header),
                       "\x00" * ctypes.sizeof(gp_header),
                       ctypes.sizeof(gp_header))

        self.assertFalse(ch_toc.verify())
        self.assertFalse(ch_settings.verify())
        self.assertFalse(gp_header.verify())
        self.assertFalse(self.spl_image_header.verify())

    def test_spl_image_header_size(self):
        self.assertEqual(len(self.spl_image_header), 520)


class TestUBootImageHeader(unittest.TestCase):

    def setUp(self):
        self.uboot_data_1 = binascii.unhexlify(
                              UBOOT_FIXTURE_1["data"].replace("\n", "").replace(" ", "")
                          )
        self.uboot_image_header = UBootImageHeader()
        self.uboot_image_header.set_bytes(self.uboot_data_1)

    def test_fields(self):
        for field, expected_value in UBOOT_FIXTURE_1["fields"].iteritems():
            actual_value = getattr(self.uboot_image_header, field)
            self.assertEqual(actual_value, expected_value, "%s is incorrect" % field)

    def test_calculate_hcrc(self):
        actual = self.uboot_image_header.calculate_hcrc()
        expected = 0x4e7da34d
        self.assertEqual(actual, expected)

    def test_verify(self):
        old_magic = self.uboot_image_header.ih_magic
        old_hcrc = self.uboot_image_header.ih_hcrc

        self.uboot_image_header.ih_magic = 0xdeadbeef
        self.uboot_image_header.ih_hcrc = 0xdeadc0ad
        self.assertFalse(self.uboot_image_header.verify())

        self.uboot_image_header.ih_magic = old_magic
        self.uboot_image_header.ih_hcrc = 0xdeadc0ad
        self.assertFalse(self.uboot_image_header.verify())

        self.uboot_image_header.ih_magic = 0xdeadbeef
        self.uboot_image_header.ih_hcrc = old_hcrc
        self.assertFalse(self.uboot_image_header.verify())

        self.uboot_image_header.ih_magic = old_magic
        self.uboot_image_header.ih_hcrc = old_hcrc
        self.assertTrue(self.uboot_image_header.verify())


class TestChunkSizeGenerator(unittest.TestCase):

    def _run_test(self, length, block_size, expected_len, 
                  expected_first, expected_last):
        block_size_list = list(chunk_size_generator(length, block_size))
        expected_sum = length
        
        actual_sum = sum(block_size_list)
        actual_len = len(block_size_list)
        self.assertEqual(actual_sum, expected_sum)
        self.assertEqual(actual_len, expected_len)
        
        # Some test runs will raise index errors.
        # Just silently ignore them.
        try:
            actual_first = block_size_list[0]
            actual_last = block_size_list[-1]
            self.assertEqual(actual_first, expected_first)
            self.assertEqual(actual_last, expected_last)
        except IndexError as e:
            pass

    def test_zero_length(self):
        self._run_test(0, 100, 0, 0, 0)

    def test_zero_remainder(self):
        self._run_test(1024, 128, 8, 128, 128)

    def test_nonzero_remainder(self):
        self._run_test(1024, 100, 11, 100, 24)

    def test_zero_block_size(self):
        with self.assertRaises(ValueError):
            self._run_test(1024, 0, 0, 0, 0)

    def test_block_size_larger_than_length(self):
        self._run_test(1024, 2048, 1, 1024, 1024)

    def test_block_size_equals_length(self):
        self._run_test(1024, 1024, 1 , 1024, 1024)


class TestChunkFileReader(unittest.TestCase):

    def setUp(self):
        self.mock_file = StringIO.StringIO()
        self.mock_file.write('\x00' * 4096)
        self.mock_file.seek(0)

    def _run_test(self, length, block_size, expected_blocks):
        self.mock_file.seek(0)
        actual_blocks = list(chunk_file_reader(self.mock_file, length, block_size))
        self.assertEqual(actual_blocks, expected_blocks)

    def test_read_nonzero_length(self):
        expected_blocks = ['\x00'*512, '\x00'*512]
        self._run_test(1024, 512, expected_blocks)

    def test_read_zero_length(self):
        expected_blocks = []
        self._run_test(0, 512, expected_blocks)

    def test_read_block_length(self):
        expected_blocks = []
        with self.assertRaises(ValueError):
            self._run_test(1024, 0, expected_blocks)

    def test_read_block_greater_than_length(self):
        expected_blocks = ['\x00'*128]
        self._run_test(128, 512, expected_blocks)

    def test_read_length_greater_than_file(self):
        expected_blocks = ['\x00'*2048, '\x00'*2048]
        self._run_test(8192, 2048, expected_blocks)