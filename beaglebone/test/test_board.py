import unittest
import binascii

from beaglebone.board import BoardIdentity
from beaglebone.board import (
    HDR_MAGIC
)


BOARD_ID_FIXTURE = {
    "data": """aa5533ee41333335424e4c54312e3041""",

    "fields": {
        "magic": 0xee3355aa,
        "name": "A335BNLT",
        "version": "1.0A",
        "serial": 0x00
    }
}

class TestBeagleBoneIdentity(unittest.TestCase):
    def setUp(self):
        hex_data = BOARD_ID_FIXTURE["data"].replace("\n", "").replace(" ", "")
        bin_data = binascii.unhexlify(hex_data)
        self.board_id = BoardIdentity.from_bytes(bin_data)

    def test_verify(self):
        self.assertTrue(self.board_id.verify())

        self.board_id.magic = 0xdeadbeef
        self.assertFalse(self.board_id.verify())

        self.board_id.magic = HDR_MAGIC
        self.assertTrue(self.board_id.verify())
